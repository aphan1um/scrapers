const _ = require('lodash');

const { htmlToText } = require('html-to-text');
const { getBody, shuffleArray, generateRandIntegers } = require('../other.js');

const SEEK_URL = 'https://www.seek.com.au';
const API_SCHEMA_NAME = 'v2_seek';
const MAX_JOBS_PER_PAGE = 22;
const MAX_SEEK_PAGES = 25;


const QueryDB = require('../queries.js');
const queryDB = new QueryDB(API_SCHEMA_NAME);

const fDelay = (x, y) => { return Math.floor(Math.random() * y) + x };
var randomDelayData;

module.exports = {
  scrape,
  API_SCHEMA_NAME
}

var currentSearchURL = new URL('/jobs', SEEK_URL).toString();
var currentCategory;

async function scrape(dbFuncs) {

  while (true) {
    [currentSearchURL, currentCategory] = await getSearchURL();
    console.log(`\n* Chosen search link: ${currentSearchURL}`);

    await collectCategories(currentSearchURL);

    // get search specific pages
    console.log(`* Now looking for page-specific search results...`);
    const searchPageURLs = await getSearchPages(currentSearchURL);
    var pagesProcessed = 0;

    const jobsPostingsToScrape = [];
    const documentsToSave = [];

    try {
      for (const u of searchPageURLs) {
        pagesProcessed += 1;

        // collect delay data per search page
        randomDelayData = await queryDB.getDelayInfo(500, 1000, logTimes = false);

        console.log(`* (${pagesProcessed}) Looking at search page ${u}`);
        console.group();

        const jobsFound = await collectJobs(u);
        jobsPostingsToScrape.push(...jobsFound);

        console.log(`- Found ${jobsFound.length} listed jobs`);
        console.groupEnd();

        await new Promise(resolve => setTimeout(resolve, 0.9 * fDelay(randomDelayData.base, randomDelayData.rand)));
      }

      console.groupEnd();

      const postingsFiltered = await dbFuncs.fFilterJobs(jobsPostingsToScrape);
      shuffleArray(postingsFiltered);

      // delay before we scrape for jobs
      console.log(`\n* Got a total of ${postingsFiltered.length}/${jobsPostingsToScrape.length} jobs`);

      console.group();

      let objJobCount = 0;
      for (const objJob of postingsFiltered) {
        objJobCount += 1;
        console.log(`- [${objJobCount}/${postingsFiltered.length}] Attempting to scrape document id ${objJob._id}:`);

        console.group();
        console.dir(objJob);

        try {
          const detailedInfo = await getDetailsFromJob(objJob['urlDetailed']);
          const fullJson = await combineJobData(objJob, detailedInfo);

          console.log(`> Listing date: ${fullJson.date.listingDate}`);
          console.log(`> Category: ${fullJson.category} (${fullJson.subcategory})`);

          // console.log(fullJson.description.dotpoints);
          // TODO: HANDLE DUPLICATE ID CASE (WHY DOES THIS HAPPEN?)

          documentsToSave.push(fullJson);
        } catch (error) {
          console.error(error);
          console.error('** Error detected! Skipping page...');
        } finally {
          console.groupEnd();
          // TODO: Not a good practice
          await new Promise(resolve => setTimeout(resolve, 0.9 * fDelay(randomDelayData.base, randomDelayData.rand)));
        }
      }

    } finally {
      console.groupEnd();
      // save batch documents (even when fatal error)
      await dbFuncs.fSaveDocuments(documentsToSave);
    }

    // update score
    const score = documentsToSave.length / jobsPostingsToScrape.length;
    await queryDB.setQueryScore(currentCategory, Number.isNaN(score) ? 0 : score);

    // adjust score based on total page-specific pages found
    // const expectedAmtPagesToCollect = await queryDB.getKey('pages-to-collect', 8, true);
    // console.log(`* INFO: ${searchPageURLs.length}/${expectedAmtPagesToCollect} search pages generated for ${currentCategory}`);
    // const pageScore = 1.0 - (searchPageURLs.length / expectedAmtPagesToCollect);
    // await queryDB.setQueryScore(currentCategory, 0, Number.isNaN(pageScore) ? Math.min((1 - 1.0/expectedAmtPagesToCollect) * 1.1, 1) : Math.pow(pageScore, 4.5));

    // wait before we search another category again
    await new Promise(resolve => setTimeout(resolve, fDelay(randomDelayData.base, randomDelayData.rand)));
  }
}

async function getSearchURL() {
  const forceMainSearch = Math.random() < (await queryDB.getKey('proba-main-search', 0, true));

  if (forceMainSearch) {
    console.log('* INFO: Forcing main search');
  }

  const selectedCategory = forceMainSearch ? '/jobs' : await queryDB.getQuery(nTopQueries = parseInt(await queryDB.getKey('top-n-queries', 5))) ?? '/jobs';
  const url = new URL(selectedCategory, SEEK_URL).toString() // + '?' + querystring.stringify(query);

  return [url, selectedCategory];
}

async function getSearchPages(baseSearchURL) {
  const $ = await getBody(baseSearchURL, false);

  const n = parseInt(await queryDB.getKey('pages-to-collect', 8));
  const probSortByDate = Math.random() < Number(await queryDB.getKey('proba-get-latest-jobs', 0.15, true));

  const jobsFound = Number($('[data-automation=totalJobsCount]').text().replace(',', ''));
  const maxPages = Math.min(MAX_SEEK_PAGES, Math.ceil(jobsFound / MAX_JOBS_PER_PAGE));
  let pageURLs;

  if (!probSortByDate) {
    pageURLs = generateRandIntegers(1, maxPages, n).map((p) => `${baseSearchURL}?page=${p}`);
  } else {
    const probGetFirstPages = Math.random() < Number(await queryDB.getKey('proba-get-latest-pages-first', 0.5, true));
    console.log(`- INFO: Getting jobs sorted by date (${(!probGetFirstPages) ? "not " : ""}by first ${n} pages)`);

    pageURLs = (probGetFirstPages)
      ? _.range(1, Math.min(n, maxPages) + 1, 1).map((p) => `${baseSearchURL}?page=${p}&sortmode=ListedDate`)
      : generateRandIntegers(1, maxPages, n).map((p) => `${baseSearchURL}?page=${p}&sortmode=ListedDate`);

    shuffleArray(pageURLs);
  }

  console.group();
  console.log(`- Total pages to collect: ${n}`);
  console.log(`- Total jobs found from base URL: ${jobsFound} jobs (approx. max pages: ${maxPages}):`);

  console.group();
  console.log(pageURLs);
  console.groupEnd();
  console.groupEnd();

  return pageURLs;
}

async function collectCategories(searchPageLink) {
  const urlPathnameLevel = new URL(searchPageLink).pathname.split('/').length - 1;
  const $ = await getBody(searchPageLink, true);

  console.log(`* Looking for categories at: ${searchPageLink}`);
  console.group();

  const searchPageLevel = $('a[role=checkbox][aria-checked=true]').length;
  console.log(`- Search page level: ${searchPageLevel} ${urlPathnameLevel}`);

  // if we don't do this we'll add too specific categories
  if (searchPageLevel > 2 || urlPathnameLevel >= 2) return;

  const redisRet = [];

  const f = (element) => {
    const $$ = $(element);
    redisRet.push(1, $$.find('a')[0].attribs.href);
  };

  if (searchPageLevel == 0) {
    $('[data-automation=item-depth-0]')
      .each((_, element) => f(element));
  }

  $('[data-automation=item-depth-1]')
    .each((_, element) => f(element));

  redisRet.push(1, '/jobs');

  console.log(`- Categories to add:`);
  console.group();
  console.log(redisRet.filter((x) => typeof x === 'string'));
  console.groupEnd();

  const amtNewCategories = await queryDB.addQueryBatch(redisRet);
  console.log(`- Found ${amtNewCategories} / ${redisRet.length} categories to add`);

  console.groupEnd();
}

async function collectJobs(searchUrl) {
  const $ = await getBody(searchUrl, false);

  const fMap = (element) => {
    const $$ = $(element);
    // const f = (tag) => $$.find(`[data-automation=${tag}]`).text();

    return {
      _id: parseInt(element.attribs['data-job-id']),
      description: {
        dotpoints: htmlToText(`<ul>${$$.find('ul').html()}</ul>`, {
          wordwrap: false
        }),
        teaser: $$.find(`[data-automation=jobShortDescription]`).text(),
      },
      urlDetailed: new URL($$.find('a[data-automation=jobTitle]')[0].attribs['href'], SEEK_URL).toString()
    }
  };

  // extract normal jobs (not featured)
  const premiumJobs = $('[data-automation=premiumJob]')
    .map((i, element) => {
      const ret = fMap(element);
      ret.premium_job = true;
      return ret;
    }).get();

  const normalJobs = $('[data-automation=normalJob]')
    .map((i, element) => fMap(element)).get();

  return normalJobs.concat(premiumJobs);
}

async function getDetailsFromJob(urlJob) {
  const $ = await getBody(urlJob, true);
  // const f = (tag) => $(`[data-automation=${tag}]`);

  var headerData = $('[data-automation=server-state]').text();
  headerData = /window\.SEEK_REDUX_DATA\s*=\s*({.*});/.exec(headerData)[1];
  headerData = eval("(" + headerData + ")")['jobdetails']['result'] // TODO: Could be dangerous

  // console.log(headerData);

  const ret = {
    // _id: parseInt(headerData[id]),
    title: headerData['title'],
    company: headerData['advertiser']['description'],
    description: {
      full: htmlToText(
        headerData['jobAdDetails'], {
        wordwrap: false
      }),
      other: headerData['roleRequirements'].join('\n'),
    },
    date: {
      listingDate: new Date(headerData['listingDate']),
      expiryDate: new Date(headerData['expiryDate'])
    },
    location: headerData['locationHierarchy'],
    salary: {
      rate: headerData['salary'],
      type: headerData['salaryType']
    },
    work_type: headerData['workType'],
    category: headerData['jobClassification'],
    subcategory: headerData['jobSubClassification'],
  }

  // console.log(ret);

  return ret;
}

async function combineJobData(summaryObj, detailedObj) {
  var ret = _.merge(summaryObj, detailedObj);

  // remove empty keys
  Object.keys(ret).forEach((key) => (ret[key] === '') && delete ret[key]);
  delete ret['urlDetailed'];

  return ret;
}
