const { generateRandIntegers, shuffleArray } = require('../other.js');
const { htmlToText } = require('html-to-text');
const _ = require('lodash');
const Xvfb = require('xvfb');

// puppeter init (with stealth plugin)
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth')();
puppeteer.use(StealthPlugin);

const cheerio = require('cheerio');

const CAREERONE_URL = 'https://www.careerone.com.au';
const CAREERONE_MAX_PAGE = 500;
const CAREERONE_MAX_JOBS_PER_PAGE = 20;

const API_SCHEMA_NAME = 'v1_careerone'

module.exports = {
  scrape,
  API_SCHEMA_NAME
}

const QueryDB = require('../queries.js');
const queryDB = new QueryDB(API_SCHEMA_NAME);


const fDelay = (x, y) => { return Math.floor(Math.random() * y) + x };
var randomDelayData;

async function scrape(dbFuncs) {
  let browser;
  if (process.env.IS_LOCAL) {
    // start puppeter locally
    browser = await puppeteer.launch({
      args: ['--incognito'],
      headless: false,
    });
  } else {
    const xvfb = new Xvfb({
      silent: false,
      timeout: 5000,
      xvfb_args: ["-screen", "0", '1920x1080x24+32', "-ac"],
    });

    console.log('[DEBUG] Starting xvfb\n');
    xvfb.startSync();

    // start puppeter
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox', '--start-fullscreen', '--incognito', '--display=' + xvfb._display],
      headless: false,
      defaultViewport: false,
    });
  }

  const browserPage = await browser.newPage();

  randomDelayData = await queryDB.getDelayInfo(800, 3500);

  while (true) {
    console.log('\nBeginning scrape iteration of CareerOne');
    console.group();

    const forceMainSearch = Math.random() < (await queryDB.getKey('proba-main-search', 0, true));

    if (forceMainSearch) {
      console.log('* INFO: Forcing main search');
    }

    let query = forceMainSearch ? '/jobs/in-australia' : await queryDB.getQuery(nTopQueries = parseInt(await queryDB.getKey('top-n-queries', 5, true))) ?? '/jobs/in-australia';

    // add chance of searching for latest jobs
    const probLatestJobs = Math.random() < (await queryDB.getKey('proba-get-latest-jobs', 0.15, true));

    if (probLatestJobs) {
      console.log(`* INFO: Will sort jobs by date for query ${query}`);
    }

    const searchPuppeterURL = new URL(query, CAREERONE_URL).toString();

    console.log(`* Using Puppeter to head to search page: ${searchPuppeterURL}`);
    await browserPage.goto(searchPuppeterURL, { waitUntil: 'networkidle2', timeout: 0 }); // remove latest from path if included

    // check if there is a 'Sign Up / Login In' pop up
    await new Promise(resolve => setTimeout(resolve, 250 + 0.75 * fDelay(randomDelayData.base, randomDelayData.rand)));
    const maybeLaterLink = await browserPage.$x("//a[descendant::text()[contains(., 'Maybe later')]]");
    if (maybeLaterLink.length > 0) {
      await new Promise(resolve => setTimeout(resolve, 1000 + 1.6 * fDelay(randomDelayData.base, randomDelayData.rand)));
      await maybeLaterLink[0].click();
    }

    await collectCategories(browserPage);

    const searchUrls = await generateSearchUrls((probLatestJobs) ? query.replace(/^\//, '/latest-') : query, browserPage);

    // tiny delay before we scrape specific pages
    await new Promise(resolve => setTimeout(resolve, 0.6 * fDelay(randomDelayData.base, randomDelayData.rand)));

    // collect jobs
    var pagesSearched = 0;

    const jobsPostingsToScrape = []
    const documentsToSave = [];

    try {
      for (let searchUrl of searchUrls) {
        randomDelayData = await queryDB.getDelayInfo(800, 3500, logTimes = false);
        pagesSearched++;

        console.log(`$ (${pagesSearched}/${searchUrls.length}) Searching page ${searchUrl.toString()}`);
        console.group();

        const searchResults = await collectJobs(searchUrl.toString(), browserPage);
        if (!searchResults) { // some error with GET request for search page
          console.groupEnd();
          await new Promise(resolve => setTimeout(resolve, 1.2 * fDelay(randomDelayData.base, randomDelayData.rand)));
          continue;
        }

        console.groupEnd();

        jobsPostingsToScrape.push(...searchResults)
        await new Promise(resolve => setTimeout(resolve, 0.75 * fDelay(randomDelayData.base, randomDelayData.rand)));
      }

      shuffleArray(jobsPostingsToScrape);
      const searchResultsFiltered = await dbFuncs.fFilterJobs(jobsPostingsToScrape, list_records = false);
      console.log(`\n* Found ${searchResultsFiltered.length}/${jobsPostingsToScrape.length} total new jobs to scrape.`)
      console.group();

      for (let [index, job] of searchResultsFiltered.entries()) {
        console.log(`${index + 1}. Searching job details at ${job.urlDetailed}`);
        const ret = await getDetailsFromJob(job.urlDetailed, browserPage);

        if (ret) {
          ret['_id'] = job['_id'];
          ret['_url'] = job.urlDetailed.toString();

          console.group();
          console.log(`> Job id: ${ret._id}`);
          console.log(`> Date posted: ${ret.date}`);
          console.groupEnd();
        }

        documentsToSave.push(ret);
        await new Promise(resolve => setTimeout(resolve, 0.85 * fDelay(randomDelayData.base, randomDelayData.rand)));
      }

      console.groupEnd();
      await new Promise(resolve => setTimeout(resolve, fDelay(randomDelayData.base, randomDelayData.rand)));

    } finally {
      // save docs (even when error)
      await dbFuncs.fSaveDocuments(documentsToSave);
    }

    // update score
    const score = documentsToSave.length / jobsPostingsToScrape.length;
    await queryDB.setQueryScore(query, score);

    // // if no queries were searched
    // const expectedAmtPagesToCollect = await queryDB.getKey('pages-to-collect', 8, true);
    // console.log(`* INFO: ${searchUrls.length}/${expectedAmtPagesToCollect} search pages generated for query ${query}`);
    // const pageScore = 1.0 - (searchUrls.length / expectedAmtPagesToCollect);
    // await queryDB.setQueryScore(query, 0, Number.isNaN(pageScore) ? Math.min(0.7, 1.1 * (1.0 - (1 / expectedAmtPagesToCollect))) : Math.pow(pageScore, 3.5));

    console.groupEnd();
  }
}


async function collectCategories(browserPage) {
  console.log('* Finding a new category to add to DB list...');
  console.group();

  await new Promise(resolve => setTimeout(resolve, 0.25 * fDelay(randomDelayData.base, randomDelayData.rand) + 150));
  await browserPage.waitForSelector('div.c1-keyword-search__dropdown', { visible: true });
  await browserPage.focus('div.c1-keyword-search__dropdown');
  await new Promise(resolve => setTimeout(resolve, 0.75 * fDelay(randomDelayData.base, randomDelayData.rand) + 500));
  await browserPage.click('div.c1-keyword-search__dropdown');
  await browserPage.waitForSelector('div.c1-keyword-search__dropdown.c1-keyword-search__dropdown--open');
  await new Promise(resolve => setTimeout(resolve, 0.75 * fDelay(randomDelayData.base, randomDelayData.rand) + 300));

  // get occupations (sub-categories) first
  let catElements = await browserPage.$$('li.job-categories__item.job-occupations__list:not(.job-categories__item--selected, .job-occupations__item--first) > span.job-occupations__job-count');
  catElements = await Promise.all(
    catElements.map(async (elHandle) => await elHandle.getProperty('parentNode'))
  );

  if (catElements.length === 0) {
    console.log('- INFO: No subcategories found; using main categories instead.');
    catElements = await browserPage.$$('li.job-categories__item.job-categories__list:not(.job-categories__item--selected)');
  } else {
    const selectedOccupations = await browserPage.$$('li.job-categories__item.job-occupations__list.job-categories__item--selected:not(.job-occupations__item--first)');

    if (selectedOccupations.length > 0) {
      console.log('- WARN: Current browser page is already at an sub-category. Skipping category search...')
      console.groupEnd();
      return;
    }
  }

  // get all category names
  const categoryNames = (await Promise.all(
    catElements.map(async (el) => await browserPage.evaluate(
      e => e.textContent,
      await el.$('span.job-categories__item-label')
    )))
  ).map((text) => text.trim());

  console.log(categoryNames);

  const indexes = [];

  (await queryDB.setIsMember('categories', categoryNames)).forEach(
    (e, i) => { if (e === 0) indexes.push(i); }
  );

  shuffleArray(indexes);

  if (indexes.length === 0) {
    console.log('- WARN: No new categories found; ending category search...');
    console.groupEnd();
    return;
  }

  const index = indexes[0];

  // a tiny delay
  await new Promise(resolve => setTimeout(resolve, 0.75 * fDelay(randomDelayData.base, randomDelayData.rand)));

  const chosenItem = catElements[index];
  await chosenItem.click();

  await browserPage.click('div.c1-keyword-search__dropdown.c1-keyword-search__dropdown--open.c1-keyword-search__dropdown--has-category');
  await browserPage.waitForNavigation();

  // strip /in-australia if included (no need, just adds Australia as location)
  const newUrl = (await browserPage.url()).trim().replace(/\/in-australia/g, '');
  console.log(`- Found new category to add: ${categoryNames[index]}. Category URL: ${newUrl}`);

  // a tiny delay
  await new Promise(resolve => setTimeout(resolve, 0.7 * fDelay(randomDelayData.base, randomDelayData.rand)));

  // add category as found
  await queryDB.addToSet('categories', categoryNames[index]);
  // add to score set
  await queryDB.addQueryBatch([1, new URL(newUrl).pathname]);

  console.groupEnd();
}

async function generateSearchUrls(query, browserPage) {

  // const results = /(.+):(.+)/.exec(query);
  // var path = '/jobs';

  // if (results) {
  //   const qType = results[1].trim();
  //   const qValue = results[2].trim().replace(/  +/g, ' ').replace(' ', '-').toLowerCase();

  //   if (qType === 'keyword') {
  //     path += `/${qValue}/jobs`
  //   } else if (qType === 'location') {
  //     path += `/in-${qValue}`;
  //   }
  // } else {
  //   path += `/in-australia`;
  // }

  // get max page number
  const baseSearchUrl = new URL(query, CAREERONE_URL).toString();
  var $;

  console.log(`* Looking at base search url: ${baseSearchUrl}`);
  console.group();

  try {
    await browserPage.goto(baseSearchUrl, { waitUntil: 'networkidle2', timeout: 0 });
    $ = cheerio.load(await browserPage.content());
  } catch (error) {
    console.error(error);
    console.error(`- ERROR: Skipping base search page ${searchUrl.toString()}`);

    console.groupEnd();
    return null;
  }

  // console.log(htmlToText($('div.srh-page').html()));
  // could be job or jobs
  const jobCountText = htmlToText($('div.srh-page').html(), { wordwrap: false });
  console.log(`- Job count text: "${jobCountText}"`);
  const searchMaxPages = Math.ceil(parseInt(/(\d+) jobs?$/.exec(jobCountText)[1]) / CAREERONE_MAX_JOBS_PER_PAGE);
  const cappedMaxPages = Math.min(searchMaxPages, CAREERONE_MAX_PAGE);
  console.log(`- Max search pages: ${searchMaxPages} (capped to ${cappedMaxPages} pages)`);

  const searchPageLevel = query.split('/').length - 1;
  const delimiter = (searchPageLevel > 1 && !query.endsWith('/in-australia')) ? '_' : '/';
  console.log(`- Search page level: ${searchPageLevel}`);

  // now generate random pages to look into
  const probGetFirstPages = Math.random() < Number(await queryDB.getKey('proba-get-latest-pages-first', 0.25, true));
  const pagesToCollect = Math.min(cappedMaxPages, parseInt(await queryDB.getKey('pages-to-collect', 8, true)));
  let result;

  if (query.startsWith('/latest-') && probGetFirstPages) {
    console.log('- INFO: Getting first few pages of search results...');
    result = _.range(1, Math.min(cappedMaxPages, pagesToCollect) + 1, 1);
    shuffleArray(result);
  } else {
    result = generateRandIntegers(1, cappedMaxPages, Math.min(cappedMaxPages, pagesToCollect));
  }

  result = result.map((p) => { return new URL(`${query}${delimiter}page_${p}`, CAREERONE_URL); });

  // print pages to log
  console.log(result.map((s) => s.toString()));
  console.groupEnd();

  return result;
}

async function collectJobs(searchUrl, browserPage) {
  var $;
  var bodyHTML;

  try {
    await browserPage.goto(searchUrl, { waitUntil: 'networkidle2', timeout: 20000 });
    // wait for search results to appear, if cloudflare appears to stop bots (wait for redirect)
    await browserPage.waitForSelector('#searchJobLoop', { timeout: 20000 });
    bodyHTML = await browserPage.content();
    $ = cheerio.load(bodyHTML);
  } catch (error) {
    console.error(error);
    throw new Error(`[ERROR] Failed to scrape search page ${searchUrl.toString()}. HTML content:\n${await browserPage.content()}`);
  }

  console.group();
  const jobCountText = htmlToText($('div.srh-page').html(), { wordwrap: false });

  if (!jobCountText) {
    throw new Error(`[ERROR] Collecting jobs at ${searchUrl} failed to get job count. HTML received:\n${bodyHTML}`);
  }

  console.log(`- Job count text: ${jobCountText}`);
  console.groupEnd();

  // console.log($.html());

  return $('div.job').map((_, e) => {
    const $$ = $(e);
    const urlPath = $$.find('a.job__title').attr('href');
    const id = /.*\/(.*)/.exec(urlPath)[1];
    const queryParamIndex = id.indexOf('?');

    return {
      _id: (queryParamIndex >= 0) ? id.substring(0, queryParamIndex) : id,
      urlDetailed: new URL(urlPath, CAREERONE_URL).toString()
    };

  }).get();
}


async function getDetailsFromJob(urlJob, browserPage) {
  var $;

  try {
    await browserPage.goto(urlJob, { waitUntil: 'networkidle2', timeout: 0 });
    $ = cheerio.load(await browserPage.content());
  } catch (error) {
    console.error(error);
    console.error(`ERROR: Skipping scrape of detailed job page ${urlJob.toString()}`);
    return null;
  }

  const ret = {};
  ret.details = {};

  $('div.job-tags-label').each((_, el) => {
    const tags = $(el.parent)
      .find('div.tw-truncate')
      .map((_, e) => { return $(e).text().trim(); })
      .get();

    switch ($(el).text().trim().toLowerCase()) {
      case 'skills':
        ret.skills = tags;
        break;
      case 'licenses & certifications':
        ret.certs = tags;
        break;
      case 'perks & benefits':
        ret.perks = tags;
        break;
    }

  });

  $('div.job-details-container').each((_, el) => {
    const $$ = $(el);
    const value = $$.find('.job-details-item__text').text().trim();

    switch ($$.find('.job-details-item__label').text().trim().toLowerCase()) {
      case 'category':
        ret.details.category = value;
        break;
      case 'sector':
        ret.details.sector = value;
        break;
      case 'occupation':
        ret.details.occupation = value;
        break;
      case 'industry':
        ret.details.industry = value;
        break;
      case 'sector':
        ret.details.sector = value;
        break;
      case 'contract type':
        ret.details.contractType = value;
        break;
      case 'company size':
        ret.details.companySize = value;
        break;
      case 'work type':
        ret.details.workType = value;
        break;
      case 'above avg.':
      case 'base pay':
        ret.details.pay = {
          type: 'base',
          amount: value
        }
        break;
      case 'similar jobs pay':
      case 'estimated':
        ret.details.pay = {
          type: 'estimate',
          amount: value
        }
        break;
      case 'date posted':
        ret.date = new Date(value);
        break;
      case 'desired education level':
        ret.details.educationLevel = value;
        break;
      case 'career level':
        ret.details.careerLevel = value;
        break;
      case 'work authorisation':
        ret.details.workAuthorisation = value;
        break;
      case 'job mode':
        ret.details.jobMode = value;
        break;
    }
  });

  ret.title = $('h1.jv-title').text().trim();
  ret.company = $('h2.jv-subtitle > span > a').text().trim();
  ret.location = $('div.jv-subtitle-location').text().replace('•', '').trim();
  ret.description = htmlToText($('div.job-text').html(), { wordwrap: false });
  return ret;
}