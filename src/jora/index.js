const { getBody, generateRandIntegers, shuffleArray, randomNum } = require('../other.js');
const { htmlToText } = require('html-to-text');
const _ = require('lodash');

const JORA_URL = 'https://au.jora.com';
const JORA_MAX_PAGES = 50;

const API_SCHEMA_NAME = 'v1_jora'

module.exports = {
  scrape,
  API_SCHEMA_NAME
}

const QueryDB = require('../queries.js');
const queryDB = new QueryDB(API_SCHEMA_NAME);

const fDelay = (x, y) => { return Math.floor(Math.random() * y) + x };
var randomDelayData;


async function scrape(dbFuncs) {

  while (true) {
    console.log('Beginning scrape of Jora');

    randomDelayData = await queryDB.getDelayInfo(1000, 4000);

    console.group();

    const forceMainSearch = Math.random() < (await queryDB.getKey('proba-main-search', 0, true));

    if (forceMainSearch) {
      console.log('* INFO: Forcing main search');
    }

    const query = (forceMainSearch) ? '/j?sp=search' : (await queryDB.getQuery(nTopQueries = parseInt(await queryDB.getKey('top-n-queries', 5, true)))) ?? '/j?sp=search';
    await findAndAddQueries(query);

    // small delay
    await new Promise(resolve => setTimeout(resolve, 0.6 * fDelay(randomDelayData.base, randomDelayData.rand)));

    const searchUrls = await generateSearchUrls(query, forceMainSearch);

    if (!searchUrls) { // no search pages generated
      console.log('- WARN: No search pages generated');
      await queryDB.setQueryScore(query, 0);
    }

    // collect jobs
    const jobsPostingsToScrape = [];
    const documentsToSave = [];

    try {
      var amtSearchPagesProcessed = 0;
      for (let searchUrl of searchUrls) {
        amtSearchPagesProcessed++;
        randomDelayData = await queryDB.getDelayInfo(1000, 4000, logTimes = false);

        console.log(`* (${amtSearchPagesProcessed}/${searchUrls.length}) Searching page ${searchUrl.toString()}`);
        console.group();

        const searchResults = await collectJobs(searchUrl.toString());
        if (!searchResults) { // some error with GET request for search page
          console.log(`- WARN: Error encountered while looking for jobs at ${searchUrl.toString()}. Skipping with penalty...`);
          await new Promise(resolve => setTimeout(resolve, fDelay(randomDelayData.base, randomDelayData.rand)));
          await queryDB.setQueryScore(query, 0);
          console.groupEnd();
          continue;
        }

        jobsPostingsToScrape.push(...searchResults);
        console.log(`- Found ${searchResults.length} listed jobs.`);
        console.groupEnd();

        await new Promise(resolve => setTimeout(resolve, 0.75 * fDelay(randomDelayData.base, randomDelayData.rand)));
      }

      shuffleArray(jobsPostingsToScrape);
      // console.log(searchResults.map((o) => o.urlDetailed.toString()));
      var searchResultsFiltered = await dbFuncs.fFilterJobs(jobsPostingsToScrape, list_records = false);

      console.log(`\n- Found ${searchResultsFiltered.length}/${jobsPostingsToScrape.length} total new jobs to scrape.`);
      console.group();

      for (let [index, job] of searchResultsFiltered.entries()) {
        console.log(`${index + 1}. Scraping job at ${job.urlDetailed.origin + job.urlDetailed.pathname}`);
        console.group();

        const ret = await getDetailsFromJob(job.urlDetailed.toString());

        if (ret) {
          ret['_id'] = job['_id'];
          ret['_url'] = job.urlDetailed.toString();

          console.log(`> Job site: ${ret.site}`);
          console.log(`> Job id: ${ret._id}`);

          // console.log(ret);
          documentsToSave.push(ret);
        }

        console.groupEnd();
        await new Promise(resolve => setTimeout(resolve, 0.7 * fDelay(randomDelayData.base, randomDelayData.rand)));
      }

      console.groupEnd();
    } finally {
      // now save jobs as a batch (even when fatal error)
      await dbFuncs.fSaveDocuments(documentsToSave);
    }

    // update score
    const score = documentsToSave.length / jobsPostingsToScrape.length
    await queryDB.setQueryScore(query, Number.isNaN(score) ? 0 : score);

    // delay before we start another iteration
    await new Promise(resolve => setTimeout(resolve, fDelay(randomDelayData.base, randomDelayData.rand)));
    console.groupEnd();

    console.log('');
  }
}


async function findQueries(urlSearchPathname, { locationQueries = true, titleQueries = false, sourceQueries = false } = {}) {
  let $;

  if (typeof urlSearchPathname === 'string') {
    const url = new URL(urlSearchPathname, JORA_URL).toString();
    $ = await getBody(url);
    console.log(`* Attempting to find queries from search page: ${url}`);
  } else {
    $ = urlSearchPathname; // if not a string, assume it's an cheerio object
  }

  var ret = []

  if (locationQueries) {
    ret = ret.concat($('a[data-sp=facet_location]')
      .map((_, e) => {
        return {
          datatype: 'facet_location',
          href: e.attribs['href']
        }
      })
      .get());
  }

  if (titleQueries) {
    ret = ret.concat($('a[data-sp=facet_title]')
      .map((_, e) => {
        return {
          datatype: 'facet_title',
          href: e.attribs['href']
        }
      })
      .get());
  }

  if (sourceQueries) {
    ret = ret.concat($('a[data-sp=facet_source]')
      .map((_, e) => {
        return {
          datatype: 'facet_source',
          href: e.attribs['href']
        }
      })
      .get());
  }

  return ret;
}

async function findAndAddQueries(urlSearchPathname) {
  const queriesFound = (await findQueries(urlSearchPathname)).map((q) => q.href);

  console.group();

  console.log(`- Found these queries:`);
  console.group();
  console.log(queriesFound);
  console.groupEnd();

  // ensure there are any queries to add
  const amtQueriesAdded = (queriesFound.length) ? await queryDB.addQueryBatch(
    queriesFound
      .map((href) => [1, href])
      .reduce((x, y) => x.concat(y), [])
  ) : 0;

  console.log(`- Added ${amtQueriesAdded}/${queriesFound.length} new queries.`);
  console.groupEnd();
}

async function generateSearchUrls(queryPath, forceMainSearch = false) {
  console.log(`* Looking at base query path: ${queryPath}`);
  console.group();

  const fQueryPath = (job, location) => `/j?q=${job}&l=${location}`;
  const fJob = (q) => /\/(.*)-jobs/.exec(q)?.[1].replace(/-/g, '+') ?? '';
  const fLocation = (q) => /-?jobs-in-(.*)/.exec(q)?.[1].replace(/-/g, '+') ?? '';

  const modifiedqueryPath = fQueryPath(fJob(queryPath), fLocation(queryPath));
  console.log(`- Query path modified to: ${modifiedqueryPath}`);
  const baseSearchUrl = new URL(modifiedqueryPath, JORA_URL);
  var $;

  try {
    $ = await getBody(baseSearchUrl.toString());
  } catch (error) {
    console.error(error);
    console.error(`* ERROR: Skipping base search page ${baseSearchUrl.toString()}`);

    console.groupEnd();
    return null;
  }

  const searchMaxPages = await getSearchMaxPages($);
  console.log(`- DEBUG: "Page title: ${$("title").text()}"`);

  if (searchMaxPages) {
    console.log(`- Search results text: "${htmlToText($('div.search-results-count').html(), { wordwrap: false })}"`);
    console.log(`- Max search pages: ${searchMaxPages}`);

    let queryPaths = [];

    if (searchMaxPages >= JORA_MAX_PAGES && !forceMainSearch) {
      console.log(`- INFO: Found excess pages, attempting to branch into sub-queries...`);

      const newQueryPaths = await findQueries($, { locationQueries: false, titleQueries: true, sourceQueries: true });
      queryPaths = queryPaths.concat(newQueryPaths.map((q) => `${fQueryPath(fJob(q.href), fLocation(queryPath))}&sp=${q.datatype}`));
    }

    console.log(`> Found these query paths to use:`);
    console.group();
    console.log(queryPaths);
    console.groupEnd();

    const numPagesToGenerate = parseInt(await queryDB.getKey('pages-to-collect', 6, true));
    const numQueriesToUse = Math.min(queryPaths.length, parseInt(await queryDB.getKey('queries-to-use', 2, true)));
    const probSortByDate = Number(await queryDB.getKey('proba-get-latest-jobs', 0.15, true)); // get latest jobs instead of 'all'

    queryPaths = _.sampleSize(queryPaths, numQueriesToUse);
    queryPaths.push(modifiedqueryPath); // also add main/base query path

    console.log(`> Sampled and using these paths to generate search pages:`);
    console.group();
    console.log(queryPaths);
    console.groupEnd();

    let results = [];
    for (let q of queryPaths) {
      const getLatestJobs = (forceMainSearch) ? true : Math.random() < probSortByDate;
      q = (getLatestJobs) ? `${q}&st=date` : q;

      const qURL = new URL(q, JORA_URL).toString();
      let qMaxPages = await getSearchMaxPages(qURL);
      const qPagesToGenerate = Math.min(qMaxPages, numPagesToGenerate);

      if (!qMaxPages) continue;

      if (!getLatestJobs || forceMainSearch) {
        results = results.concat(generateRandIntegers(1, qMaxPages, qPagesToGenerate).map((p) => `${qURL}&p=${p}`));
      } else {
        const tmp = _.range(1, qPagesToGenerate + 1, 1).map((p) => `${qURL}&p=${p}`);
        shuffleArray(tmp);

        results = results.concat(tmp);
      }
    }

    console.log(`> Finalised search pages:`);
    console.group();
    console.log(results);
    console.groupEnd();

    console.groupEnd();
    return results;

  } else {
    const message = htmlToText($('div.message').html(), { wordwrap: false });

    console.groupEnd();

    if (message.toLowerCase().includes("we couldn't find any jobs that matched your search") || message.toLowerCase().includes("we have looked through all the results for you")) {
      console.warn(`- WARN: Search page does not contain any search results, skipping...`);
      return null;
    }

    throw `ERROR: Undefined error when searching base page. ${message}`;
  }
}

async function getSearchMaxPages(url) {
  let $;

  if (typeof url === 'string') {
    await new Promise(resolve => setTimeout(resolve, 0.45 * fDelay(randomDelayData.base, randomDelayData.rand)));
    $ = await getBody(url);
  } else {
    $ = url;
  }

  const result = /Page \d+ of (\d+)/.exec(htmlToText($('div.search-results-count').html(), { wordwrap: false }));
  return result ? parseInt(result[1]) : null;
}

async function collectJobs(searchUrl) {
  var $;

  try {
    $ = await getBody(searchUrl);
  } catch (error) {
    console.error(error);
    console.error(`ERROR: Skipping scrape of search page ${searchUrl.toString()}`);
    return null;
  }

  console.log(`- Search results text: "${htmlToText($('div.search-results-count').html(), { wordwrap: false })}"`);
  console.log(`- Page title: ${$("title").text()}`);

  const initialJobList = $('article.organic-job')
    // .filter((_, e) => {
    //     return $($(e).find('a.job-item')).attr('data-ga-label') === 'seek.com.au';
    // })
    .map((_, e) => {
      const $$ = $(e);

      return {
        _id: $$.attr('id'),
        site: $$.find('a.job-item').attr('data-ga-label'),
        urlDetailed: new URL($$.find('a.job-link').attr('href'), JORA_URL)
      };

    })
    .get()
    .filter((o) => !o.urlDetailed.pathname.startsWith('/job/rd/'))

  // avoid seek jobs since we're already scraping with separate module
  const purgedSeekJobList = initialJobList.filter((o) => o.site !== 'seek.com.au');

  console.log(`- Found ${initialJobList.length - purgedSeekJobList.length} seek.com.au jobs purged / ${initialJobList.length} total jobs`);
  // console.dir(purgedSeekJobList);

  return purgedSeekJobList;
}


async function getDetailsFromJob(urlJob) {
  var $;

  try {
    $ = await getBody(urlJob);
  } catch (error) {
    console.error(error);
    console.error(`ERROR: Skipping scrape of detailed job page ${urlJob.toString()}`);
    return null;
  }

  let company = $('span.company').text().trim();
  let description;
  let title;
  let isJoraLocal = (company === 'Jora Local');

  if (isJoraLocal) {
    console.log('Jora local advert detected');
    ({ title, description } = await getJoraLocalDetails(new URL($('a.apply-button').attr('href'), JORA_URL).toString()));
    company = title.match(/ at (.+)$/)[1].trim();
  } else {
    description = htmlToText(
      $('#job-description-container').html(),
      { wordwrap: false }
    );
    title = $('.job-title').text().trim();
  }

  return {
    title,
    jobType: $('div.badge').text().replace(/\s\s+/g, ' ').trim(), // TODO: Very rough prototype
    age: parseDate($('.listed-date').text().trim()),
    site: $('span.site').text().trim(),
    location: $('span.location').text().trim(),
    company,
    description,
    isJoraLocal
  }
}

async function getJoraLocalDetails(urlJoraLocal) {
  var $;
  try {
    $ = await getBody(urlJoraLocal);
  } catch (error) {
    console.error(error);
    console.error(`ERROR: Skipping scrape of Jora local page ${urlJoraLocal.toString()}`);
    return null;
  }

  return {
    title: $("[data-test-key='positionHeader']").text().trim(),
    description: htmlToText($(".PositionDescription__columnLeft").html(), { wordwrap: false }).trim(),
  };
}


function parseDate(dateStr) {
  const timeAgo = /(\d*) ([a-z]+) ago/.exec(dateStr.toLowerCase());
  const dateType = timeAgo[2];
  const value = parseInt(timeAgo[1]);
  const ret = new Date();

  if (dateType.startsWith('day'))
    ret.setDate(ret.getDate() - value);
  else if (dateType.startsWith('minute'))
    ret.setMinutes(ret.getMinutes() - value);
  else if (dateType.startsWith('hour'))
    ret.setHours(ret.getHours() - value);
  else if (dateType.startsWith('month'))
    ret.setMonth(ret.getMonth() - value);

  console.log(`> ${dateStr} ${value} => ${ret}`);

  return ret;
}