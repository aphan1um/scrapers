const redis = require('redis');
const promisify = require('util').promisify;
const { randomNum } = require('./other.js');

console.log(`Attempting to connect to Redis DB: ${process.env.REDIS_URL}`);

const client = redis.createClient({
  'host': process.env.REDIS_URL,
  'port': Number(process.env.REDIS_PORT || 6379)
});

const DEFAULT_LEARNING_RATE = 0.12;

const get = promisify(client.get).bind(client);
const set = promisify(client.set).bind(client);
const zadd = promisify(client.zadd).bind(client);
const zincrby = promisify(client.zincrby).bind(client);
const zscore = promisify(client.zscore).bind(client);
const sendCommand = promisify(client.sendCommand).bind(client);
const hincrby = promisify(client.hincrby).bind(client);
const sadd = promisify(client.sadd).bind(client);
const incr = promisify(client.incr).bind(client);
const zrem = promisify(client.zrem).bind(client);
const zrevrangebyscore = promisify((key, count, cb) => client.zrevrangebyscore(
  key, '+inf', '-inf', 'LIMIT', 0, count, (err, results) => cb(err, results)
)).bind(client);
const hset = promisify(client.hset).bind(client);

client.on("error", function (error) {
  console.error(error);
});

class QueryDB {
  // uses the simple UCB bandit algorithm 
  constructor(name) {
    this.name = name;
  }

  async getKey(key, defaultValue, forceAdd = false) {
    const val = await get(`${this.name}:${key}`);

    if (!val && forceAdd)
      await set(`${this.name}:${key}`, defaultValue);

    return val ?? defaultValue;
  }

  async getDelayInfo(defaultBase, defaultRand, logTimes = true) {
    var base = await get(`${this.name}:wait-base`);
    var rand = await get(`${this.name}:wait-rand`);

    if (!base) {
      await set(`${this.name}:wait-base`, defaultBase);
      base = defaultBase;
    }

    if (!rand) {
      await set(`${this.name}:wait-rand`, defaultRand);
      rand = defaultRand;
    }

    const ret = {
      base: parseInt(base),
      rand: parseInt(rand)
    };

    if (logTimes) {
      console.log(`* Using these wait times: ${JSON.stringify(ret)}`);
    }

    return ret;
  }

  async setIsMember(key, items) {
    // TODO: Performance issue
    return await sendCommand('smismember', [`${this.name}:${key}`, ...items]);
  }

  async addToSet(key, member) {
    await sadd(`${this.name}:${key}`, member);
  }

  async addQueryBatch(queries) {
    // NX prevents new samples being added
    return await zadd(`${this.name}:scores`, 'NX', ...queries);
  }

  // NOTE: DO NOT USE the query "default" (use for example 'keyword:default' instead)
  async getQuery(nTopQueries = 3) {
    var chosenQuery;
    var chosenQueryScore;
    
    console.log(`* Finding a query to search... (using top ${nTopQueries} queries)`);
    console.group();

    const topQueries = await zrevrangebyscore(`${this.name}:scores`, nTopQueries);
    chosenQuery = topQueries[parseInt(randomNum(0, topQueries.length - 1))] ?? "default";
    chosenQueryScore = await zscore(`${this.name}:scores`, chosenQuery) ?? 0;

    if (chosenQuery === "default") {
      console.log(`- Chosen default query with score ${chosenQueryScore}.`);
    } else {
      console.log(`- Chosen query "${chosenQuery}" with score ${chosenQueryScore}.`);
    }
    
    console.groupEnd();

    return (chosenQuery === "default") ? null : chosenQuery;
  }

  async setQueryScore(query, score, learningRateOverride) {
    if (isNaN(score)) {
      return score;
    }
    
    if (!query) {
      query = "default";
    }

    await hincrby(`${this.name}:times-visited`, query, 1);
    await incr(`${this.name}:total-visits`);
    await hset(`${this.name}:last-visited`, query, new Date().toLocaleString("en-AU", {timeZone: "Australia/Melbourne"})); // record date

    new Date().toLocaleString("en-AU", {timeZone: "Australia/Melbourne"})

    const oldScore = Number((await zscore(`${this.name}:scores`, query)) ?? 0);
    const learningRate = learningRateOverride ?? await this.getKey('learning-rate', DEFAULT_LEARNING_RATE, true);
    const newScore = this._getUpdatedScore(oldScore, score, learningRate);

    await zincrby(`${this.name}:scores`, newScore, query);
    console.log(`Search scrape performance for query ${query}: ${score} (${oldScore} => ${oldScore + newScore}) (learning rate ${learningRate})`);

    return newScore;
  }

  _getUpdatedScore(oldScore, newScore, learn_rate) {
    // zincrby apparently adds to the existing score so don't need to add oldScore
    return learn_rate * (newScore - oldScore);
  }
}

module.exports = QueryDB;