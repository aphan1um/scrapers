const cheerio = require('cheerio');
const axios = require('axios').default;
const _ = require('lodash');

function isEmpty(str) {
  return (!str || str.length === 0);
}

// Credit to https://stackoverflow.com/a/6274381
function shuffleArray(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }

  return a;
}

async function getBody(link, log = false) {
  if (log) {
    console.log(`> Retrieving web page from ${link}...`);
  }

  const result = await axios.get(link, {timeout: 5000});
  return cheerio.load(result.data);
}

function decodeEscapeSequence(str) {
  return str;
}

function randomNum(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Credit to https://stackoverflow.com/a/50652249
function generateRandIntegers(min, max, n) {
  const nums = new Set();
  
  // if this isn't done, can cause infinite loop below
  if (n >= max - min + 1) {
    return _.range(min, max + 1, 1);
  }

  while (nums.size !== n) {
    // credit to https://stackoverflow.com/a/7228322
    nums.add(randomNum(min, max));
  };

  return [...nums];
}

module.exports = {
  isEmpty,
  shuffleArray,
  getBody,
  decodeEscapeSequence,
  generateRandIntegers,
  randomNum
}