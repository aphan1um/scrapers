const api = require(`./${process.argv[2].toLowerCase()}`);
const http = require('http');
const pg = require('pg');
const pg_format = require('pg-format');

let lastScraped = Date.now();
const MAX_WAIT_SCRAPE = 60 * 60 * 1000 // 1 hour or 60 minutes

console.log(`Scraping using API: ${process.argv[2].toLowerCase()}`);

const requestListener = function (req, res) {
  res.writeHead((Date.now() - lastScraped < MAX_WAIT_SCRAPE) ? 201 : 404);
  res.write(process.argv[2].toLowerCase());
  res.end();
}

const pgClient = new pg.Pool({
  user: process.env.PG_USERNAME,
  database: process.env.PG_DBNAME,
  port: parseInt(process.env.PG_PORT, 10),
  host: process.env.PG_HOST,
  password: process.env.PG_PASSWORD,
});

const server = http.createServer(requestListener);
server.listen(parseInt(process.argv[3].trim(), 10));

console.log(`Created health endpoint at port: ${process.argv[3].trim()}`);

// Get recognised and their job sites and their ids
(async () => {
  const jobsites = await (await pgClient.query('SELECT jobsite_id,title FROM infojobsite')).rows;
  const jobsite_id = jobsites.find((el) => el.title == `${api.API_SCHEMA_NAME}s`).jobsite_id;

  console.dir(jobsites);

  const fSaveDocuments = async (objs) => {
    // if no documents added
    if (objs.length == 0) {
      return true;
    }

    const rows_to_add = objs.map((ob) => {
      const ob_clone = Object.assign({}, ob);
      delete ob_clone._id;
      return [
        jobsite_id,
        ob._id.toString(),
        ob_clone
      ];
    });

    
    try {
      await pgClient.query(
        pg_format(
          'INSERT INTO jobs(jobsite_id, job_web_id, data) VALUES %L ON CONFLICT (jobsite_id, job_web_id) DO NOTHING',
          rows_to_add
        ),
        []
      );
    } catch (e) {
      throw e;
    }

    lastScraped = Date.now();
    return true;
  };

  const fFilterJobs = async (objs, list_records = true) => {
    // TODO: Convert numbers to strings?
    const ids = objs.map((o) => o._id.toString());

    const records = (await pgClient.query('SELECT job_web_id FROM jobs WHERE jobsite_id = $1 AND job_web_id = ANY ($2)',
      [jobsite_id, ids]))
      .rows
      .map((el) => el.job_web_id);
    
    if (list_records) {
      console.dir(records);
    }

    return objs.filter((o) => records.indexOf(o._id.toString()) < 0);
  };

  api.scrape({ fFilterJobs, fSaveDocuments }).catch((e) => {
    console.error(e);
    process.exit(-1);
  });
})();
